package PasswordValidatorPackageTest;

import static org.junit.Assert.*;

import org.junit.Test;

import PasswordValidatorPackage.PasswordValidator;

public class PasswordValidatorTest {

	@Test
	public void testIsValidLength() 
	{
		boolean result = 
				PasswordValidator.isValidLength( "1234567890" ); 
		assertTrue ( "Invalid length" , result);
	}

	@Test
	public void testIsValidLengthException() 
	{
		boolean result = PasswordValidator.isValidLength( " " ); 
		assertFalse ( "Invalid length" , result);
	}

	@Test
	public void testIsValidLengthExceptionSpace() 
	{
		boolean result = PasswordValidator.isValidLength( "    t   e   s    t   " ); 
		assertFalse ( "Invalid length" , result);
	}

	@Test
	public void testIsValidLengthBoundryIn() 
	{
		boolean result = PasswordValidator.isValidLength( "MoreThen8" ); 
		assertTrue ( "Invalid length" , result);
	}

	@Test (expected=StringIndexOutOfBoundsException.class)
	public void testIsValidLengthBoundryOut() 
	{
		boolean result = PasswordValidator.isValidLength( "eight" ); 
		fail ( "Invalid length");
	}
	

	// A password must contain at least two digits
	
	@Test
	public void testIsContainsTwoDigits() 
	{
		boolean result = PasswordValidator.isContainsTwoDigits("myDigitIs86592");
		assertTrue("Does not contain 2 digits", result);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testIsContainsTwoDigitsException() 
	{
		boolean result = PasswordValidator.isContainsTwoDigits("myDigitIs");
		fail("Does not contain 2 digits");
		
	}

	@Test
	public void testIsContainsTwoDigitsBoundryIn()
	{
		//Testing boundary in, here I consider only 2 digit is boundary in
		boolean result = PasswordValidator.isContainsTwoDigits("myDigitIs92");
		assertTrue("Does not contain 2 digits", result);
	}
	
	
	
	@Test (expected=NumberFormatException.class)
	public void testIsContainsTwoDigitsBoundrayOut()
	{
		//Testing boundary out, here I consider only 1 digit is boundary out
		boolean result = PasswordValidator.isContainsTwoDigits("myDigitIs2");
		fail("Does not contain 2 digits");
	}
	
}
