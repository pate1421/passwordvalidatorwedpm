package PasswordValidatorPackage;


/**
 * 
 * @author Bhaumik patel
 *
 */
public class PasswordValidator {
	
	private static int MIN_LENGTH = 8;
	
	public static boolean isValidLength(String password) throws StringIndexOutOfBoundsException
	{
		if( password.trim().length() < MIN_LENGTH)
		{
			throw new StringIndexOutOfBoundsException();
		}
		return password.indexOf(" ") < 0; 
	}
	
	// A password must contain at least two digits
	public static boolean isContainsTwoDigits(String password) throws NumberFormatException
	{
		int num = 0;
		boolean bol = false;
		for(int i = 0; i < password.length(); i++) {
			if(Character.isDigit(password.charAt(i))) {
				num++;
			}
			if(num >= 2) {
				bol = true;
				return true;
			}
		}
		if (bol == false) {
			throw new NumberFormatException();
		}
		return false;
	}

}










//return password.trim().length() >= MIN_LENGTH;//&& password.length() >= MIN_LENGTH;